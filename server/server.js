"use strict";
var express = require("express");
var app = express();
var fs = require("fs");
var morgan = require("morgan"); // log requests to the console (express4)
var bodyParser = require("body-parser"); // pull information from HTML POST (express4)
var methodOverride = require("method-override"); // simulate DELETE and PUT (express4)
var path = require("path");

app.use(express.static(__dirname + "/public")); // set the static files location /public/img will be /img for users
app.use(morgan("dev")); // log every request to the console
app.use(bodyParser.urlencoded({ extended: "true" })); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: "application/vnd.api+json" })); // parse application/vnd.api+json as json
app.use(methodOverride());

var fileContent = fs.readFileSync("pai-bot-usage-data.json");
var content = JSON.parse(fileContent);

app.get("/api/chartData", function(req, res) {
  var returnData = {};
  var positiveSentiments = {
    key: "Positive",
    color: "#4f99b4",
    values: []
  };
  var negativeSentiments = {
    key: "Negative",
    color: "#d67777",
    values: []
  };
  // Grouping Data and calculating mean
  content.forEach(function(_obj) {
    if (returnData[_obj.Category]) {
      if (_obj.Sentiment > 0) {
        returnData[_obj.Category].positiveMean += _obj.Sentiment;
        returnData[_obj.Category].positiveCount++;
      } else if (_obj.Sentiment < 0) {
        returnData[_obj.Category].negativeMean += _obj.Sentiment;
        returnData[_obj.Category].negativeCount++;
      }
    } else {
      returnData[_obj.Category] = {
        positiveMean: _obj.Sentiment > 0 ? _obj.Sentiment : 0,
        negativeMean: _obj.Sentiment < 0 ? _obj.Sentiment : 0,
        positiveCount: 0,
        negativeCount: 0
      };
    }
  });
  console.log(returnData);
  // Preparing data grouped and filtered to positive and negative
  Object.keys(returnData).forEach(function(key) {
    positiveSentiments.values.push({
      label: key,
      value: returnData[key].positiveMean/returnData[key].positiveCount
    });
    negativeSentiments.values.push({
      label: key,
      value: returnData[key].negativeMean/returnData[key].negativeCount
    });
  });
  returnData = [];
  returnData.push(positiveSentiments);
  returnData.push(negativeSentiments);
  res.json(returnData);
});

/* app.use('/components', express.static(__dirname + '/../app/components')); */
app.use(
  "/bower_components",
  express.static(__dirname + "/../app/bower_components")
);
/* app.use('/modules', express.static(__dirname + '/../app/modules'));
app.use('/factories', express.static(__dirname + '/../app/factories')); */
app.use("/", express.static(__dirname + "/../app/"));

/* app.get("*", function(req, res) {
  res.sendFile(path.resolve("./../app/index.html")); // load the single view file (angular will handle the page changes on the front-end)
});
 */
// listening to app server
app.listen(8000);
console.log("App listening on port 8000");
