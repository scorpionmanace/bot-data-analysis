(function() {
  "use strict";

  var app = angular.module("botData");

  app.factory("botDataFactory", botDataFactory);
  /**
   * Returns the data to process the charts and description about them
   * @param {*}
   */
  function botDataFactory($http) {
    function getChartData() {
      return $http
        .get("/api/chartData")
        .success(function(res) {
          return res;
        })
        .error(function() {
          return error;
        });
    }
    return {
      getChartData: getChartData
    };
  }
})();
