'use strict';

describe('botData home module', function() {

  beforeEach(module('botData'));

  describe('home controller', function(){

    it('should ....', inject(function($controller) {
      //spec body
      var homeComponent = $controller('homeComponent');
      expect(homeComponent).toBeDefined();
    }));

  });
});