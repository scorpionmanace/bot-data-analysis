(function() {
  "use strict";
  var app = angular.module("botData", ["ngRoute"]);

  app.config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider.when("/", {
        template: "<home-component></home-component>"
      });
    }
  ]);

  app.directive("homeComponent", function() {
    return {
      templateUrl: "/modules/home/home.html",
      controller: homeController,
      controllerAs: "hctrl"
    };
  });

  function homeController() {}
})();
