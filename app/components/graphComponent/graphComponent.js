(function() {
  "use strict";

  var app = angular.module("botData");

  app.directive("graphComponent", function() {
    return {
      templateUrl: "/components/graphComponent/graphComponent.html",
      scope: {
        graphData: "=?"
      },
      controller: graphController,
      controllerAs: "grctrl"
    };
  });

  function graphController(botDataFactory) {
    var jsonData;
    botDataFactory.getChartData().then(function(res) {
      jsonData = res.data;
      nv.addGraph(function() {
        var chart = nv.models
          .multiBarHorizontalChart()
          .x(function(d) {
            return d.label;
          })
          .y(function(d) {
            return d.value;
          })
          .margin({ top: 30, right: 20, bottom: 50, left: 175 })
          .showValues(true)
          .tooltips(false)
          .showControls(false);

        chart.yAxis.tickFormat(d3.format(",.2f"));

        d3.select("#chart svg")
          .datum(jsonData)
          .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
      });
    });
  }
})();
